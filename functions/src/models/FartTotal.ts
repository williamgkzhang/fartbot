import db from "../services/firestoreService";
import {firestore} from "firebase-admin";
const {FieldValue} = firestore;
/**
 * Represents the FartTotal collection and count in Firestore
 */
export default class FartTotal {
  static initial = {count: 0}
  static docRef = db.collection("fartTotal").doc("total")

  /**
   * Retrieves the count from Firestore
   */
  static async get(): Promise<number> {
    const totalSnap = await this.docRef.get();
    const data = totalSnap.data();
    if (data == null) {
      return 0;
    } else {
      return data.count;
    }
  }

  /**
   * Increments the fart counter by a number
   * @param {number} farts amount to increase the fart counter by
   * @return {Promise} the new number of farts
   */
  static async increment(farts: number): Promise<number> {
    await this.docRef.update({
      count: FieldValue.increment(farts),
    });
    return await this.get();
  }

  /**
   * Sets the firestore collection to the initial count if
   * it doesn't exist
   */
  static async initializeIfNeeded(): Promise<void> {
    const totalSnap = await this.docRef.get();
    if (!totalSnap.exists) {
      await this.docRef.set(this.initial);
    }
  }
}

FartTotal.initializeIfNeeded();
