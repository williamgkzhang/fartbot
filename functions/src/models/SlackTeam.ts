import * as functions from "firebase-functions";
import db from "../services/firestoreService";
import {WebClient} from "@slack/web-api";

/**
 * Model for modifying and reading slack team information
 */
export default class SlackTeam {
  static collectionRef = db.collection("slackTeams")

  /**
   * generate the oauth redirecturl with scopes
   * @return {string} the oauth redirecturl with scopes and client id
   */
  static redirectUrl() : string {
    const scopes = [
      "channels:history",
      "chat:write",
      "team:read",
      "users:read",
    ];
    const clientId = functions.config().slack.client_id;
    const url = `https://slack.com/oauth/v2/authorize?scope=${scopes.join(",")}&client_id=${clientId}`;

    return url;
  }

  /**
   * adds a slack team to firestore
   * @param {string} code the authorization code returned from slack
   * @return {Promise} promise after complete
   */
  static async add(code: string) : Promise<void> {
    const result = await (new WebClient()).oauth.v2.access({
      client_id: functions.config().slack.client_id,
      client_secret: functions.config().slack.client_secret,
      code,
    });

    const teamId = result.team!.id;

    const existingTeams = await this.collectionRef.where("teamId", "==", teamId)
        .get();
    existingTeams.forEach(async (team) => await team.data().delete());

    const newTeamRef = this.collectionRef.doc();
    await newTeamRef.set({
      teamId: teamId,
      teamName: result.team!.name,
      tokenType: result.token_type,
      accessToken: result.access_token,
      scopes: result.scope,
    });
  }

  /**
   * gets a slack team
   * @param {string} teamId the id of the team to get
   * @return {Promise} promise with team data
   */
  static async getTeam(teamId: string) {
    const existingTeams = await this.collectionRef.where("teamId", "==", teamId)
        .get();

    return existingTeams.docs[0].data();
  }
}
