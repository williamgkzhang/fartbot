import db from "../services/firestoreService";

/**
 * Represents the FartTotal collection and count in Firestore
 */
export default class FartLog {
  static collectionRef = db.collection("fartLog")

  /**
   * Adds a log entry to the fart log
   * @param {number} numberOfFarts the number of farts to increment
   * @param {string} userId Slack user id
   * @param {string} teamId Slack team/workspace id
   * @return {Promise} promise after finished
   */
  static async add(
      numberOfFarts: number,
      userId: string,
      teamId: string,
  ): Promise<void> {
    const docRef = this.collectionRef.doc();
    await docRef.set({
      numberOfFarts,
      userId,
      teamId,
      createdAt: new Date(),
    });
  }
}
