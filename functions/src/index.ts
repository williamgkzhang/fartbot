import * as functions from "firebase-functions";
import "./services/firebaseApp";
import {WebClient} from "@slack/web-api";

import FartTotal from "./models/FartTotal";
import FartLog from "./models/FartLog";
import SlackTeam from "./models/SlackTeam";

const fartIncrementRegex = /^farts\s*[+]\s*([0-9]{1,2})/i;

/**
 * Processes base event
 * @param {Object} event the damn event
 * @param {Object} response handle the response
 */
async function processEvent(
    event: any, //eslint-disable-line
    response: functions.Response<any> //eslint-disable-line
) {
  const {type, user, team, text, channel} = event.event;
  if (type === "message" && typeof text === "string") {
    functions.logger.info("Message Received", {text});

    const incrementMatches = event.event.text.match(fartIncrementRegex);
    if (incrementMatches != null) {
      const incrementNumber = parseInt(incrementMatches[1]);
      const totalFarts = await logFart(incrementNumber, user, team);

      await respondWithTotalFartsSlack(team, channel, totalFarts);
    }

    response.send("ok");
  } else {
    response.send("Unknown");
  }
}

/**
 * Responds on slack with fart count
 * @param {string} team Slack team id
 * @param {string} channel Slack channel id
 * @param {number} totalFarts New total of farts
 */
async function respondWithTotalFartsSlack(
    team: string,
    channel:string,
    totalFarts:number
) {
  const slackTeamDetails = await SlackTeam.getTeam(team);
  const client = new WebClient(slackTeamDetails.accessToken);

  client.chat.postMessage({
    channel: channel,
    text: `:peach: ${totalFarts} farts :dash:`,
  });
}

/**
 * Increments fart counter and logs fart
 * @param {number} numberOfFarts the number of farts to increment
 * @param {string} userId Slack user id
 * @param {string} teamId Slack team/workspace id
 * @return {Promise} promise representing new number of farts
 */
async function logFart(
    numberOfFarts: number,
    userId: string,
    teamId: string,
): Promise<number> {
  const totalFarts = await FartTotal.increment(numberOfFarts);
  await FartLog.add(numberOfFarts, userId, teamId);

  return totalFarts;
}

export const slackEventsHandler = functions.https.onRequest(
    async (request, response) => {
      const eventType = request.body.type;
      // functions.logger.info("Event Received", {body: request.body});

      if (eventType === "url_verification") {
        response.type("text");
        response.send(request.body.challenge);
      } else if (eventType === "event_callback") {
        processEvent(request.body, response);
      } else {
        response.send("Unknown");
      }
    }
);

export const oauthRedirectHandler = functions.https.onRequest(
    async (request, response) => {
      functions.logger.info(
          "oauth redirect Received",
          {body: request.body, query: request.query}
      );
      if ("code" in request.query) {
        SlackTeam.add(request.query.code as string);
      }
      response.send(SlackTeam.redirectUrl());
    }
);
