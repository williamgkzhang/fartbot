import {WebClient} from "@slack/web-api";
import * as functions from "firebase-functions";

// Read a token from the environment variables
const token = functions.config().slack.bot_token;

// Initialize
const client = new WebClient(token);
export default client;
